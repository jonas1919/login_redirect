<?php
/**
 * Implements hook_form_alter().
 */
function hook_user_login($account) {
  // We want to redirect user on login.
  $response = new RedirectResponse("localhost/user/{%}");
  $response->send();
  return;
}